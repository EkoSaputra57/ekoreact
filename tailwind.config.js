module.exports = {
  purge: ["./src/**/*.{js,jsx,ts,tsx}", "./public/index.html"],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      height: {
        xl: "600px",
        md: "400px",
      },
      width: {
        md: "400px",
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
