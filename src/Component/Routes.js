import React from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import ES from "../Assets/ES.jpg";

function Routes() {
  return (
    <div className="flex justify-between mx-2 my-2 font-bold xl:text-xl">
      <div>
        <img src={ES} style={{ height: 30, width: 40 }} />
      </div>
      <div
        style={{
          color: "rgb(96, 165, 250) ",
        }}
      >
        <Link to="/" className="spaces">
          Home
        </Link>

        <Link to="/about" className="spaces">
          About
        </Link>

        <Link to="/contact" className="spaces">
          Contact
        </Link>
      </div>
    </div>
  );
}

export default Routes;
