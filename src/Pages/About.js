import React, { useState } from "react";
import Eko from "../Assets/Eko.png";
import "../Style/Home.css";
import { DownCircleTwoTone } from "@ant-design/icons";
import Dmovie from "../Assets/LogoBlue.jpg";
import CrowdFinder from "../Assets/CrowdFinder.png";

function About() {
  return (
    <div>
      <div className="BGstyle2 flex h-96 mb-4 xl:h-xl">
        <div className=" flex flex-1 text-center justify-center flex-column text-lg font-extrabold pt-10 xl:text-4xl">
          <p>I Am Ready To Create Your Mobile Application</p>
        </div>
      </div>
      <div className="flex-1 flex  items-center justify-center flex-row mx-4 mb-8 xl:text-2xl xl:ml-24 xl:mr-24">
        <p
          className="font-bold"
          style={{
            color: "rgb(110,101,101) ",
          }}
        >
          Completed 2 projects as student in Glints Academy intensive bootcamp
          and achieving satisfactory result.
        </p>
        <img
          src={Eko}
          style={{ height: 100, width: 100, borderRadius: 1000, margin: 20 }}
        />
      </div>
      <div className="border-4 font-bold rounded-md w-1/2 text-center ml-4 flex-1 flex  items-center justify-center flex-row xl:w-80 xl:text-xl xl:ml-32">
        <p className="mr-2" style={{ color: "rgb(96, 165, 250)" }}>
          check out my work
        </p>
        <DownCircleTwoTone />
      </div>
      <div className=" mt-8 flex-1 flex  items-center justify-evenly flex-row xl:pb-12">
        <a
          target="_blank"
          href="https://drive.google.com/file/d/1lgiv1xnMKvvmNwzIZxMaEOxM1bwndpGu/view?usp=sharing"
        >
          <img
            src={Dmovie}
            style={{ width: 70, height: 70, borderRadius: 10 }}
          />
        </a>

        <a
          target="_blank"
          href="https://drive.google.com/file/d/1FncYoAxqIWdg-1vBjnqu8G6s6rkfy7I4/view?usp=sharing"
        >
          <img src={CrowdFinder} style={{ width: 200, height: 50 }} />
        </a>
      </div>
    </div>
  );
}

export default About;
