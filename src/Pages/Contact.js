import Contacts from "../Assets/Contact.jpg";
import Line from "../Assets/Line.png";
import WA from "../Assets/WA.png";

export default function Contact() {
  return (
    <div>
      <div className="flex flex-1  items-center justify-center text-center BGstyle3 h-96 xl:h-xl">
        <div>
          <p
            className="font-bold text-4xl mt-8 xl:text-7xl"
            style={{
              color: "rgb(110,101,101) ",
            }}
          >
            Contact Me
          </p>
          <p
            className="text-sm mt-4 mb-8 xl:text-xl"
            style={{
              color: "rgb(110,101,101) ",
            }}
          >
            Let's know each other more.
          </p>
          <div className="items-center justify-center flex flex-1 flex-col">
            <p className="flex flex-1 flex-row items-center ml-2 mb-4 font-bold xl:text-xl xl:mb-8">
              <img src={WA} style={{ height: 30, width: 30, marginRight: 5 }} />
              +62 813 9680 3975
            </p>
            <p className="flex flex-1 flex-row items-center ml-2 font-bold xl:text-xl">
              <img
                src={Line}
                style={{ height: 30, width: 30, marginRight: 5 }}
              />
              vialdolorasa
            </p>
          </div>
        </div>
      </div>
      <div className=" flex flex-1 justify-center">
        <img
          src={Contacts}
          style={{
            height: 300,
            width: 300,
            margin: 20,
          }}
        />
      </div>
    </div>
  );
}
