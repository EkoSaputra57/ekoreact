import "../Style/Home.css";
import MidPic from "../Assets/Mid.gif";
import ES from "../Assets/ES.jpg";
import React from "react";
import { useHistory } from "react-router-dom";

export default function Home() {
  const history = useHistory();

  const handleRoute = () => {
    history.push("/about");
  };
  return (
    <div>
      <div className="BGstyle flex h-64 xl:h-xl xl:mb-8">
        <div className="flex-1 flex  items-center justify-center flex-col text-lg font-extrabold xl:text-4xl">
          <p
            style={{
              color: "rgb(110,101,101) ",
            }}
            className="xl:mb-4"
          >
            Hello There!
          </p>
          <p
            style={{
              color: "rgb(110,101,101) ",
            }}
            className="xl:mb-4"
          >
            I Am Eko Saputra
          </p>
          <p
            className="px-2"
            style={{
              color: "rgb(96, 165, 250) ",
              borderWidth: 3,
              borderRadius: 8,
              borderColor: "blue",
            }}
          >
            Mobile App Developer
          </p>
        </div>
      </div>
      <div className="flex-1 flex  items-center justify-center flex-col text-lg font-bold my-2 xl:text-4xl">
        <p
          style={{
            color: "rgb(96, 165, 250) ",
          }}
        >
          What did I do?
        </p>
      </div>
      <div className=" justify-center flex flex-1">
        <img src={MidPic} style={{ height: 300, width: 400 }} />
      </div>
      <div>
        <p className="flex-1 flex  text-center justify-center  text-lg font-bold m-2 xl:text-3xl">
          Innovation Solutions to Expand Your Creative Project to Reach Mobile
          App User
        </p>
        <p className="flex-1 flex  text-center justify-center text-gray-400 m-2 xl:text-lg">
          I am Eko Saputra, a mobile app developer in Indonesia. I specialize in
          helping company grow their businesses
        </p>
        <p
          className="flex-1 flex  text-center justify-center font-bold  text-lg  mb-8"
          style={{
            color: "rgb(96, 165, 250) ",
          }}
        >
          React Native Mobile App Developer
        </p>
        <div className="flex-1 flex  items-center justify-evenly px-3 pb-8">
          <button class="bg-blue-500 hover:bg-blue-700 text-white font-bold  py-2 px-4 rounded-full xl:py-4 xl:px-8">
            <a
              target="_blank"
              href="https://drive.google.com/file/d/1FncYoAxqIWdg-1vBjnqu8G6s6rkfy7I4/view?usp=sharing"
            >
              View my work
            </a>
          </button>
          <button
            onClick={handleRoute}
            class="bg-blue-500 hover:bg-blue-700 text-white font-bold  py-2 px-4 rounded-full xl:py-4 xl:px-8"
          >
            More Info
          </button>
        </div>
        <div className="flex-1 flex  items-center justify-center my-2">
          <img src={ES} style={{ height: 30, width: 40 }} />
        </div>
      </div>
    </div>
  );
}
